const express = require('express')
const path = require('path')
const pg = require('pg');
const PORT = process.env.PORT || 5000
const app = express();
const pool = new pg.Pool()


app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/', (req, res) => res.render('pages/index'));

app.get('/db', (request, response) => {
  pool.connect(process.env.DATABASE_URL, (err, client, done) => {
    client.query('SELECT * FROM test_table', (err, result) => {
      done();
      if (err) {
      	console.error(err); response.send("Error " + err);
      } else {
      	response.render('pages/db', {results: result.rows} );
      }
    });
  });
});

app.listen(PORT, () => console.log(`Listening on ${ PORT }`));